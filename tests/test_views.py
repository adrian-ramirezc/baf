from baf import models
from baf.views.default import my_view
from baf.views.notfound import notfound_view

def test_my_view_success(app_request, dbsession):
    info = my_view(app_request)
    assert app_request.response.status_int == 200
    assert info['count'] == 0
    assert info['project'] == 'baf'

def test_notfound_view(app_request):
    info = notfound_view(app_request)
    assert app_request.response.status_int == 404
    assert info == {}
